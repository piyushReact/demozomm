import React from 'react';
import {View, Text} from 'react-native';

export default function BorderDash(props) {
  return (
    <View
      style={[
        {
          borderStyle: 'dashed',
          borderWidth: 1,
        },
        {...props.style},
      ]}
    />
  );
}
