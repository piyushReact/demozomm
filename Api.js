import axios from 'axios';

const axiosInstance = axios.create();
let apiHeaders = {
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
};
export const getApi = async (url, header, successCallback, errorCallback) => {
  axiosInstance
    .get(url, header)
    .then((response) => {
      successCallback(response);
    })
    .catch((error) => {
      errorCallback(error);
    });
};
