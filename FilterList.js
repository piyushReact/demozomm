import React from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd9gfs6-145571e29d72',
    title: '4 Item',
  },
  {
    id: '58694a0f-3da1-f471f-bd96-145571e29d72',
    title: '5 Item',
  },
  {
    id: '58694a0f-3da1-471fgf-bd96-145571e29d72',
    title: '6 Item',
  },
  {
    id: '58694a0f-3da1-471f-bdfsg96-145571e29d72',
    title: '7 Item',
  },
  {
    id: '58694a0f-3da1-471f-bsfgfsd96-145571e29d72',
    title: '8 Item',
  },
];

const Item = ({title, index}) => (
  <>
    {
      <View
        style={[styles.item, {marginRight: DATA.length - 1 == index ? 80 : 0}]}>
        <Text style={styles.title}>{title}</Text>
      </View>
    }
  </>
);
export default function BorderDash(props) {
  const renderItem = ({item, index}) => (
    <Item title={item.title} index={index} />
  );

  return (
    <View style={[{...props.style}]}>
      <FlatList
        data={DATA}
        horizontal
        showsHorizontalScrollIndicator={false}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
      <View
        style={{
          position: 'absolute',
          backgroundColor: 'white',
          right: -25,
          padding: 5,
          bottom: 10,
          paddingVertical: 8,
          paddingHorizontal: 20,
          borderLeftWidth: 1,
          borderTopWidth: 1,
          borderBottomWidth: 1,
          borderRadius: 10,
        }}>
        <Text>{'Popular'}</Text>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    // backgroundColor: '#f9c2ff',
    padding: 8,
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 10,
    marginVertical: 8,
    marginHorizontal: 5,
    flexDirection: 'row',
  },
  title: {
    fontSize: 15,
  },
});
