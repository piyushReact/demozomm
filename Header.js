import React, {useRef, useEffect} from 'react';
import {View, Text, Image, TextInput, Animated} from 'react-native';
import BorderDash from './BorderDash';
import FilterListComponent from './FilterList';

/**
 *
 * we can create TextInput box as a common component but here we are taking as a example and writing
 * code like this to complete the task in less time
 *
 * @export
 * @param {*} props
 * @returns
 */
export default function Header(props) {
  const locationAnimation = useRef(new Animated.Value(0)).current;
  useEffect(() => {
    Animated.timing(locationAnimation, {
      toValue: props.yState > 200 ? -50 : 0,
      duration: 600,
    }).start();
  }, [props.yState]);

  const {SearchBox, FilterList} = props;
  const Header_Height = 180;
  const Temp_Location = 'Bangalore Palace, Vasant nagar near silk board';
  const Temp_Placeholder_Name = 'Search name, Cuisine or dish';
  return (
    <View
      style={[{backgroundColor: 'white', height: Header_Height, padding: 20}]}>
      <View
        style={[
          {
            width: '70%',
            flexDirection: 'row',
          },
        ]}>
        <Image
          source={require('./assets/location.png')}
          style={{height: 25, width: 25}}
        />
        <View style={{paddingLeft: 10}}>
          <Text numberOfLines={1} style={{fontSize: 20, fontWeight: 'bold'}}>
            {Temp_Location}
          </Text>
          <BorderDash />
        </View>
      </View>
      <Animated.View
        style={[
          {
            backgroundColor: 'white',
            transform: [{translateY: locationAnimation}],
          },
        ]}>
        {SearchBox ? (
          <View
            style={{
              marginTop: 20,
              width: '100%',
              alignSelf: 'center',
              borderWidth: 1,
              flexDirection: 'row',
              justifyContent: 'flex-start',
              alignItems: 'center',
              borderRadius: 10,
              borderColor: 'grey',
              paddingHorizontal: 15,
              height: 48,
            }}>
            <Image
              style={{height: 25, width: 25}}
              source={require('./assets/search.png')}
            />

            <TextInput
              placeholder={Temp_Placeholder_Name}
              style={{
                fontSize: 18,

                paddingLeft: 20,
                height: 48,
              }}
            />
          </View>
        ) : null}
        {FilterList ? <FilterListComponent /> : null}
      </Animated.View>
    </View>
  );
}
