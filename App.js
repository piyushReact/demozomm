import React, {useState, useEffect} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
} from 'react-native';

import {Colors} from 'react-native/Libraries/NewAppScreen';
import Header from './Header';
import Restaurant from './Restaurant';
import {getApi} from './Api';

const App = () => {
  const [yState, setYState] = useState(0);

  useEffect(() => {
    fetchRestaurantList();
  }, []);

  const fetchRestaurantList = () => {
    const url =
      'https://developers.zomato.com/api/v2.1/collections?lat=22.5645&lon=72.9289&count=10';

    const AuthToken = 'Bearer 5ffb698e3d9a8ea8d51fb8847c216058';

    let apiHeaders = {
      headers: {
        Accept: 'application/json',
        Authorization: AuthToken,
      },
    };
    //usually we use redux so, then i am using different handled rest service
    //api call service. there we call api in action and handling status and other
    // api lavel validation
    /*
    Imp note is that i am hitting the api call here but due to city id and token saying expired.
    reason being not able t impliment the next thing.
    but as per the done thing you can see observe the stage of mine.
    $$ due to short of timing at my place not able to complete it, now having some other
    extra work from the current company.
    Thanks
    */
    getApi(url, apiHeaders, successListGroup, errorApi);
  };

  const successListGroup = (success) => {
    console.log('success check ', success);
  };

  const errorApi = (error) => {
    console.log('error check ', error);
  };

  const _onLayout = ({nativeEvent}) => {
    setYState(nativeEvent.contentOffset.y);
  };

  return (
    <>
      <StatusBar barStyle="dark-content" />
      <SafeAreaView>
        <Header SearchBox FilterList yState={yState} />
        <ScrollView
          onScroll={_onLayout}
          contentInsetAdjustmentBehavior="automatic"
          style={styles.scrollView}>
          <View style={styles.body}>
            <Restaurant />
          </View>
        </ScrollView>
      </SafeAreaView>
    </>
  );
};

const styles = StyleSheet.create({
  scrollView: {
    backgroundColor: Colors.lighter,
  },
  engine: {
    position: 'absolute',
    right: 0,
  },
  body: {
    backgroundColor: Colors.white,
  },
  sectionContainer: {
    marginTop: 32,
    paddingHorizontal: 24,
  },
  sectionTitle: {
    fontSize: 24,
    fontWeight: '600',
    color: Colors.black,
  },
  sectionDescription: {
    marginTop: 8,
    fontSize: 18,
    fontWeight: '400',
    color: Colors.dark,
  },
  highlight: {
    fontWeight: '700',
  },
  footer: {
    color: Colors.dark,
    fontSize: 12,
    fontWeight: '600',
    padding: 4,
    paddingRight: 12,
    textAlign: 'right',
  },
});

export default App;
