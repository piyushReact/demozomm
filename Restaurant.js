import React from 'react';
import {View, Text, FlatList, StyleSheet} from 'react-native';

const DATA = [
  {
    id: 'bd7acbea-c1b1-46c2-aed5-3ad53abb28ba',
    title: 'First Item',
  },
  {
    id: '3ac68afc-c605-48d3-a4f8-fbd91aa97f63',
    title: 'Second Item',
  },
  {
    id: '58694a0f-3da1-471f-bd96-145571e29d72',
    title: 'Third Item',
  },
  {
    id: '58694a0f-3da1-471f-bd9gfs6-145571e29d72',
    title: '4 Item',
  },
  {
    id: '58694a0f-3da1-f471f-bd96-145571e29d72',
    title: '5 Item',
  },
  {
    id: '58694a0f-3da1-471fgf-bd96-145571e29d72',
    title: '6 Item',
  },
  {
    id: '58694a0f-3da1-471f-bdfsg96-145571e29d72',
    title: '7 Item',
  },
  {
    id: '58694a0f-3da1-471f-bsfgfsd96-145571e29d72',
    title: '8 Item',
  },
];

const Item = ({title, index}) => (
  <>
    {
      <View style={[styles.item]}>
        <Text style={styles.title}>{title}</Text>
      </View>
    }
  </>
);
export default function Restaurant(props) {
  const renderItem = ({item, index}) => (
    <Item title={item.title} index={index} />
  );

  return (
    <View style={[{...props.style}]}>
      <FlatList
        data={DATA}
        renderItem={renderItem}
        keyExtractor={(item) => item.id}
      />
    </View>
  );
}

const styles = StyleSheet.create({
  item: {
    height: 250,
    width: '88%',
    alignSelf: 'center',
    padding: 8,
    marginTop: 20,
    borderWidth: 1,
    borderRadius: 15,
    marginVertical: 8,
    marginHorizontal: 5,
    flexDirection: 'row',
  },
  title: {
    fontSize: 15,
  },
});
